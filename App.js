import React from "react";
import Button from "./components/Button.js";
import Modal from "./components/Modal.js";
import './App.scss';

class App extends React.Component { 
    constructor(props) { 
      super(props);
      this.leftBtn = this.leftBtn.bind(this);
      this.rightBtn = this.rightBtn.bind(this);
      this.test = this.test.bind(this);
      this.closeModal1 = this.closeModal1.bind(this);
      this.closeModal2 = this.closeModal2.bind(this);

      this.state = {
        visible1: false,
        visible2: false,
        shadow: false
      };

      this.actions = <div className="actions">
        <div className="ok">Ok</div>
        <div className="cancel">Cancel</div>
      </div>;

      this.actions2 = <div className="actions">
        <div className="ok">I will!</div>
        <div className="cancel">Not today</div>
      </div>;
    }
  
    leftBtn() {
      // console.log("left");
      this.setState({
        visible1: true,
        visible2: false,
        shadow: true
      });
    }
  
    rightBtn() {
      // console.log("right");
      this.setState({
        visible2: true,
        visible1: false,
        shadow: true
      });
    }
  
    test(e) {
    // console.log(e.target.id);
    if (e.target.id === "test") { 
      this.setState({
        visible1: false,
        visible2: false,
        shadow: false
      });
    }
    }
  
  closeModal1() { 
    this.setState({
        visible1: false,
        shadow: false
      });
  }

  closeModal2() { 
    this.setState({
        visible2: false,
        shadow: false
      });
  }
  
    render() { 
    return (
    <div className="App" onClick={this.test} id="test">
      <Button backgroundColor="lightgreen" text="Open first modal" onClick={this.leftBtn} id="btn1" />
      <Button backgroundColor="rgba(231, 76, 60, 1)" text="Open second modal" onClick={this.rightBtn} id="btn2" />
      {this.state.shadow && <div className="shadow" onClick={this.test} id="test"></div>}
      {this.state.visible1 && <Modal
        text="Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
        header="Do you want to delete this file?"
        closeButton={true}
        actions={this.actions}
        closeModal={this.closeModal1}
        id={1} />}
      {this.state.visible2 && <Modal
        text="Do not do this!"
        header="STOP!"
        closeButton={true}
        actions={this.actions2}
        closeModal={this.closeModal2}
        id={2} />}
    </div>
  );   
    }
}

export default App;