import React from "react";
import "./Modal.scss";

class Modal extends React.Component { 
    constructor(props) { 
        super(props);
    }

    render() { 
       return (
        <div className="modal" id={this.props.id}>
            <div className="header_all">
                <p className="header">{this.props.header}</p>
                <div className="close" onClick={this.props.closeModal}>
                    {this.props.closeButton && <div className="line line1"></div>}
                    {this.props.closeButton && <div className="line line2"></div>}
                </div>
            </div>
            <p className="text">{this.props.text}</p>
            <div>{this.props.actions}</div>
        </div>
        )
    }
}

export default Modal;