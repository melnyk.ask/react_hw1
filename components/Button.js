import React from "react";
import "./Button.scss";

class Button extends React.Component { 
    constructor(props) { 
        super(props);
    }

    render() { 
       return (
        <>
            <button
                className="button"
                id={this.props.id}
                style={{ backgroundColor: this.props.backgroundColor }}
                onClick={this.props.onClick}>
                    {this.props.text}
            </button>
        </>
    ) 
    }
}

export default Button;